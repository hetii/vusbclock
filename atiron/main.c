#include <avr/eeprom.h>
#include "irmp.h"

#define UE40J5100_PLAY      0xB847
#define UE40J5100_REC       0xB649
#define UE40J5100_SUPPORT   0xC03F

uint16_t EEMEM EEint;

static void timer1_init (void){
  #if defined (__AVR_ATtiny45__) || defined (__AVR_ATtiny85__)                // ATtiny45 / ATtiny85:

  #if F_CPU >= 16000000L
      OCR1C   =  (F_CPU / F_INTERRUPTS / 8) - 1;                              // compare value: 1/15000 of CPU frequency, presc = 8
      TCCR1   = (1 << CTC1) | (1 << CS12);                                    // switch CTC Mode on, set prescaler to 8
  #else
      OCR1C   =  (F_CPU / F_INTERRUPTS / 4) - 1;                              // compare value: 1/15000 of CPU frequency, presc = 4
      TCCR1   = (1 << CTC1) | (1 << CS11) | (1 << CS10);                      // switch CTC Mode on, set prescaler to 4
  #endif

  #else                                                                       // ATmegaXX:
      OCR1A   =  (F_CPU / F_INTERRUPTS) - 1;                                  // compare value: 1/15000 of CPU frequency
      TCCR1B  = (1 << WGM12) | (1 << CS10);                                   // switch CTC Mode on, set prescaler to 1
  #endif

  #ifdef TIMSK1
      TIMSK1  = 1 << OCIE1A;                                                  // OCIE1A: Interrupt by timer compare
  #else
      TIMSK   = 1 << OCIE1A;                                                  // OCIE1A: Interrupt by timer compare
  #endif
}

#ifdef TIM1_COMPA_vect                                                      // ATtiny84
#define COMPA_VECT  TIM1_COMPA_vect
#else
#define COMPA_VECT  TIMER1_COMPA_vect                                       // ATmega
#endif

ISR(COMPA_VECT){
  (void) irmp_ISR();
}

void hdd_on(void){
  PORTB |= (1<<PB0);
}

void hdd_off(void){
  PORTB &= ~(1<<PB0);
}

void htpc_off(void){
  PORTB |= (1<<PB0);
}

void htpc_on(void){
  PORTB &= ~(1<<PB0);
}

int main (void){
  IRMP_DATA   irmp_data;

  DDRB |= (1<<PB0);
  hdd_off();
  irmp_init();
  timer1_init();
  sei ();
  // eeprom_write_word(0, 0x1234)
  //eeprom_write_word(&EEint, 0x1234);

  for (;;){
    if (irmp_get_data(&irmp_data)){
      //eeprom_write_word(&EEint, irmp_data.command);
      if (irmp_data.command == UE40J5100_PLAY){
      //if (irmp_data.command == UE40J5100_PLAY){
        hdd_on();
        _delay_ms(400);
        hdd_off();
      }
    }
  }
}
