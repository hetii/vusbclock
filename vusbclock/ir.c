#include "ir.h"
#include <avr/eeprom.h>
#include <avr/interrupt.h>

static IRMP_DATA irmp_data;
static IRMP_DATA last_irmp_data;
static IRMP_DATA trained_irmp_data[CODES_TO_TRAIN] EEMEM;

/*
  0: Disabled
  1: Enabled
  2: Ready to save last recived command.
*/
static uint8_t ir_training_mode;
static uint8_t current_codes;

char *cmd_title[CODES_TO_TRAIN] = { "ON/OFF 1", "ON/OFF 2", "Clock / Temp 1", "Clock / Temp 2"};

static uint8_t are_equal(IRMP_DATA *p1, IRMP_DATA *p2){
    return ((p1->protocol == p2->protocol)  &&  (p1->address == p2->address) &&  (p1->command == p2->command));
}

// Its frustrating but its f*cking C ... 
static void setstr(char **pp, char *p, char v){
  if (v != 0){
    p[0] = v;
    p[1] = 0;
  }
  *pp = p;
}

ISR(TIMER2_COMP_vect){
  (void) irmp_ISR(); 
}

void ir_init(uint8_t mode){
  ir_training_mode = mode;
  current_codes = 0;

  OCR2  =  (uint8_t) (((F_CPU / F_INTERRUPTS) / 8) - 1 + 0.5);  // Compare Register OCR2
  TCCR2 = (1 << WGM21) | (1 << CS21);                           // CTC Mode, prescaler = 8
  TIMSK |= 1 << OCIE2;      
}

void ir_accept(void){
  ir_training_mode = 2;
}

int8_t ir_pool(){
  // Learning mode.
  if (ir_training_mode > 0){
    if (current_codes < CODES_TO_TRAIN){
      if ( irmp_get_data (&irmp_data) && (irmp_data.flags & IRMP_FLAG_REPETITION) && (!are_equal(&irmp_data, &last_irmp_data)) ){
        memcpy(&last_irmp_data, &irmp_data, sizeof(irmp_data));
        //sprintf(str, "\nCurrent code %d address %04x command %04x", current_codes, irmp_data.address, irmp_data.command);
        //sprintf(str, "\nNr %d address %04x command %04x", current_codes, irmp_data.address, irmp_data.command);
        sprintf(str, "\nNr %d for [%s] address %04x command %04x", current_codes, cmd_title[current_codes], irmp_data.address, irmp_data.command);
        // Reset pointer.
        setstr(&s, str, 0);

      //ir_training_mode = 2; // Ready to save, see below.
      //} else if (BUTTON_DOWN && ir_training_mode == 2) {
      } else if (ir_training_mode == 2) {
        ir_training_mode = 1;
        s = " [ OK ]";
        //sprintf(s, "\nSaving code: %d, address: %04x, command: %04x", current_codes, irmp_data.address, irmp_data.command);
        eeprom_update_block(&irmp_data, &trained_irmp_data[current_codes++], sizeof(irmp_data));
      }
    } else {
      s = " [ OK ]\nWork done - switch to normal mode ...";
      ir_training_mode = 0;
    }
  // Code for regular mode.
  } else if (irmp_get_data (&irmp_data)){
    // Go throught our stored data
    for (current_codes = 0; current_codes < CODES_TO_TRAIN; ++current_codes){
      // Read eeprom value to SRAM last_irmp_data variable.
      eeprom_read_block(&last_irmp_data, &trained_irmp_data[current_codes], sizeof(irmp_data));
      // If match, then set ir_data to current index for future evaluating in main function.
      if (are_equal(&irmp_data, &last_irmp_data)){
        return current_codes;
      }
    }
  }
  return -1;
}
