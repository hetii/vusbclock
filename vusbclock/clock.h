#ifndef __clock_h_included__
#define __clock_h_included__
#include "display.h"
typedef volatile unsigned char BYTE;
typedef volatile unsigned int WORD;

BYTE minutes;
BYTE seconds;
BYTE hours;

void clock_start(void);
void clock_stop(void);
void clock_init(void);
#endif