#!/usr/bin/env python

import usb, datetime, time, threading
from usb import USBError

class VUSB(object):

    def __init__(self, vendor_id=0x16c0, product_id=0x05dc):
        self.vendor_id = vendor_id
        self.product_id = product_id
        self.thread = None

    @property
    def dev(self):
        return usb.core.find(idVendor=self.vendor_id, idProduct=self.product_id)

    def _set_time(self, h=None, m=None, s=None):
        if h or m or s:
            self.dev.ctrl_transfer(usb.TYPE_VENDOR|usb.RECIP_DEVICE|usb.ENDPOINT_IN, 1, h, 0, 1)
            self.dev.ctrl_transfer(usb.TYPE_VENDOR|usb.RECIP_DEVICE|usb.ENDPOINT_IN, 2, m, s, 1)
        else:
            d = datetime.datetime.now()
            self.dev.ctrl_transfer(usb.TYPE_VENDOR|usb.RECIP_DEVICE|usb.ENDPOINT_IN, 1, d.hour, 0, 1)
            self.dev.ctrl_transfer(usb.TYPE_VENDOR|usb.RECIP_DEVICE|usb.ENDPOINT_IN, 2, d.minute, d.second, 1)
    
    def set_time(self, *arg, **kw):
	try:
            self._set_time(*arg, **kw)
        except USBError as e:
            print 'error',e
            time.sleep(1)
            self.set_time(*arg, **kw)

    def get_time(self):
        while True:
            try:
                d=datetime.datetime.now()
                res = dev.ctrl_transfer(usb.TYPE_VENDOR|usb.RECIP_DEVICE|usb.ENDPOINT_IN, 0, 0, 0, 1)
                if res.tolist() != [d.hour, d.minute, d.second]:
                    print res.tolist(), "is not equal", [d.hour, d.minute, d.second]
                time.sleep(0.5)
            except Exception as e:
                pass

    def _get_ir_msg(self):
        s = ''
        while True:
            try:
                res = self.dev.ctrl_transfer(usb.TYPE_VENDOR|usb.RECIP_DEVICE|usb.ENDPOINT_IN, 3, 0, 0, 250).tolist()
                string = ''.join([chr(i) for i in res]).strip()
                if string and s != string:
                    print string
                    s = string
                time.sleep(0.2)
            except Exception as e:
                pass

    def get_ir_msg(self):
        if self.thread is None:
            self.thread = threading.Thread(target=self._get_ir_msg)
            self.thread.start()

    def stop_ir_msg(self):
        self.thread.stop()
        self.thread = None

    def training_mode(self):
        self.dev.ctrl_transfer(usb.TYPE_VENDOR|usb.RECIP_DEVICE|usb.ENDPOINT_IN, 4, 0, 0, 10)

    def accept_ir_code(self):
        self.dev.ctrl_transfer(usb.TYPE_VENDOR|usb.RECIP_DEVICE|usb.ENDPOINT_IN, 5, 0, 0, 10)

    def display_value(self, val):

        val = str(val)
        dot = val.find('.')
        val = val.replace('.','')

        x = 0
        if dot == 1: x = 8
        if dot == 2: x = 4
        if dot == 3: x = 2
        if dot == 4: x = 1

        self.dev.ctrl_transfer(usb.TYPE_VENDOR|usb.RECIP_DEVICE|usb.ENDPOINT_IN, 7, x, 0, 10)
        time.sleep(0.1)
        self.dev.ctrl_transfer(usb.TYPE_VENDOR, 6, int(val[0:2]), int(val[2:4]), 10)

    def enable_clock(self):
        self.dev.ctrl_transfer(usb.TYPE_VENDOR|usb.RECIP_DEVICE|usb.ENDPOINT_IN, 8, 0, 0, 10)

    def upgrade(self):
        self.dev.ctrl_transfer(usb.TYPE_VENDOR|usb.RECIP_DEVICE|usb.ENDPOINT_IN, 9, 0, 0, 10)

if __name__ == "__main__":
    v = VUSB()
    #v.set_time()
    v.upgrade()
