#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <avr/eeprom.h>
#include <util/delay.h>
#include <avr/wdt.h>
#include <avr/io.h>
#include "usbdrv.h"
#include "oddebug.h"
#include "clock.h"
#include "ir.h"

// Definition: dev.ctrl_transfer(bmRequestType,                        bRequest,  wValue=0,             wIndex=0, data_or_wLength=None, timeout=None)
//                                                                     on/off     duration              port
// dev.ctrl_transfer(usb.TYPE_VENDOR|usb.RECIP_DEVICE|usb.ENDPOINT_IN, 2,         0,                    3 )
//                                                                     bRequest   rq->wValue.bytes[0]   rq->wIndex.bytes[0] & 7;

USB_PUBLIC uchar usbFunctionSetup(uchar data[8]){

  usbRequest_t *rq = (void *)data;
  static uchar replyBuf[3];
  usbMsgPtr = replyBuf;

  switch(rq->bRequest){
    case 0: // Read actual time.
      replyBuf[0] = hours;
      replyBuf[1] = minutes;
      replyBuf[2] = seconds;
      return 3;
      break;
    case 1: // Set just hour.
      hours = rq->wValue.bytes[0];
      break;
    case 2:// Set minutes and second and start clock
      minutes = rq->wValue.bytes[0];
      seconds = rq->wIndex.bytes[0];
      clock_start();
      break;
    case 3: // Get last ir message.
      usbMsgPtr = (unsigned char *) s;
      return strnlen((char *)usbMsgPtr, 150);
      break;
    case 4: // Learn ir code.
      ir_init(1);
      break;
    case 5: // Accept last ir code.
      ir_accept();
      break;
    case 6: // Disable clock and display raw value.
      display_one = rq->wValue.bytes[0];
      display_two = rq->wIndex.bytes[0];
      display_mode = 0;
      break;
    case 7: // Enable clock on display.
      display_dot = rq->wValue.bytes[0];
      break;
    case 8: // Enable clock on display.
      display_mode = 1;
      break;
    case 9: // Jump to bootloader.
      eeprom_write_byte ((uint8_t*) 2000, 64);
      for(;;){}
      break;
    default:
      return 0;
  }
  return 0;
}

static void usb_init(void){
  uchar   i = 0;

  usbInit();
  /* enforce USB re-enumerate: */
  usbDeviceDisconnect();  /* do this while interrupts are disabled */

  while(--i){         /* fake USB disconnect for > 250 ms */
    wdt_reset();
    _delay_ms(1);
  }
  usbDeviceConnect();
}

int main(void){
          //76543210
  DDRB  = 0b00111110;
  DDRC  = 0b00111111;
  DDRD  = 0b01100010;
  PORTD = 0b00100010;

  // Off all digits.
  PORTB &= ~((1<<PB1)|(1<<PB2));
  PORTC &= ~((1<<PC4)|(1<<PC3));

  ir_init(0);
  clock_init();
  display_one = 3;
  display_two = 45;
  uint8_t i=0;

  for (i = 0; i < 10; ++i){  
    PORTB ^= (1<<3);
    _delay_ms(250);
  }

  wdt_enable(WDTO_1S);

  usb_init();
  sei();

  int8_t data = -1;
  //main event loop
  for(;;){
    wdt_reset();
    usbPoll();
    data = ir_pool();
    switch(data){
      case 0:
      case 1:
        PORTD |= (1<<6);
        _delay_ms(250);   
        PORTD &= ~(1<<6);
        break;
      case 2:
      case 3:
        display_mode ^= 1;
        break;
      default:
        break;
    }
  }
  return 0;
}
