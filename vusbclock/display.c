#include "display.h"

/*
  K1: PB2  K2: PB1  K3: PC3  K4  PC4

        A PC1
      ---------
F PC2 |       | B PC0
      | G PD1 |          * PB 3
      ---------
E PC5 |       | C PB5
      |       |          *
      --------- 
        D PD5   * H PB4
*/

static uint8_t digits[10] = {192, 249, 164, 176, 153, 146, 130, 248, 128, 144};

void set_segment(uint8_t nr){
  uint8_t val = digits[nr];
  uint8_t i = 0;
  while (i < 8) {
    if (val & 0x01) {
      switch (i) {
        case 0:PORTC |= (1<<1);break; // A
        case 1:PORTC |= (1<<0);break; // B
        case 2:PORTB |= (1<<5);break; // C
        case 3:PORTD |= (1<<5);break; // D
        case 4:PORTC |= (1<<5);break; // E
        case 5:PORTC |= (1<<2);break; // F
        case 6:PORTD |= (1<<1);break; // G
        //case 7:PORTB |= (1<<4);break; // H
        //case 8:PORTB |= (1<<4);break; // Dot
      }
    } else {
      switch (i) {
        case 0:PORTC &= ~(1<<1);break; // A
        case 1:PORTC &= ~(1<<0);break; // B
        case 2:PORTB &= ~(1<<5);break; // C
        case 3:PORTD &= ~(1<<5);break; // D
        case 4:PORTC &= ~(1<<5);break; // E
        case 5:PORTC &= ~(1<<2);break; // F
        case 6:PORTD &= ~(1<<1);break; // G
        //case 7:PORTB &= ~(1<<4);break; // H
        //case 8:PORTB &= ~(1<<4);break; // Dot
      }
    }
    i++;
    val = val >> 1;
  }
}

void set_digit(uint8_t first, uint8_t second, uint8_t dot){
  static uint8_t segment=1;

  if (dot == segment) PORTB &= ~(1<<4); else PORTB |= (1<<4);

  switch (segment) {
    case 8:    
      if ((first / 10) == 0) break;
      set_segment(first / 10);
      PORTB &= ~((1<<PB1)|(1<<PB2));
      PORTC |= (1<<PC4);
      PORTC &= ~(1<<PC3);
    break;
    case 4:
      set_segment(first % 10);
      PORTB &= ~((1<<PB1)|(1<<PB2));
      PORTC |= (1<<PC3);
      PORTC &= ~(1<<PC4);
    break;
    case 2:
      set_segment(second / 10);
      PORTB |= (1<<PB2);
      PORTB &= ~(1<<PB1);
      PORTC &= ~((1<<PC4)|(1<<PC3));
    break;
    case 1:
      set_segment(second % 10);
      PORTB |= (1<<PB1);
      PORTB &= ~(1<<PB2);
      PORTC &= ~((1<<PC4)|(1<<PC3));
    break;
  }
  segment = (segment<<1)>8?1:segment<<1;
}

/*
void set_digit(uint16_t val, uint8_t dot){
  static uint8_t segment=1;
  static uint16_t digit;

  if (digit == 0){
    digit = val;
    segment = 1;
  }

  set_segment(digit % 10);

  if (dot == segment) PORTB &= ~(1<<4); else PORTB |= (1<<4);

  switch (segment) {
    case 8:
      PORTB &= ~((1<<PB1)|(1<<PB2));
      PORTC |= (1<<PC4);
      PORTC &= ~(1<<PC3);
    break;
    case 4:
      PORTB &= ~((1<<PB1)|(1<<PB2));
      PORTC |= (1<<PC3);
      PORTC &= ~(1<<PC4);
    break;
    case 2:
      PORTB |= (1<<PB2);
      PORTB &= ~(1<<PB1);
      PORTC &= ~((1<<PC4)|(1<<PC3));
    break;
    case 1:
      PORTB |= (1<<PB1);
      PORTB &= ~(1<<PB2);
      PORTC &= ~((1<<PC4)|(1<<PC3));
    break;
  }
  digit /= 10;
  segment = (segment<<1)>8?1:segment<<1;
}
*/