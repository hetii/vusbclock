#include <avr/interrupt.h>
#include <string.h>
#include "clock.h"

ISR(TIMER0_OVF_vect){
  // If clock enabled.
  if (display_mode == 1){
    set_digit(hours, minutes, 0);
  } else {
    set_digit(display_one, display_two, display_dot);
  }
}

ISR(TIMER1_COMPA_vect){
  if (display_mode == 1){
    PORTB ^= (1<<3);
  } else {
    PORTB &= ~(1<<3);
  }
  if(++seconds>=60){
    seconds=0;
    if(++minutes>=60){
      minutes=0;
      if(++hours>=24){
        hours=0;
      }
    }
  }
}

void clock_start(void){  
  // Configuration for clock timer1:
  TCCR1B |= (1<<CS12)|(1<<WGM12); //P = 256, CTC mode.
  OCR1A = 46875;
  TIMSK |= (1 << OCIE1A);
}

void clock_stop(void){
  TCCR1B = 0x0;
  TIMSK &= ~(1 << OCIE1A);
  OCR1A = 0x00;
}

void clock_init(void){
  minutes=0;
  seconds=0;
  hours=0;
  display_mode = 1;

  // Configuration for display timer0:
  TCCR0 |= (1<<CS02); // P = 256, Normal mode.
  TIMSK |= (1<<TOIE0);
  clock_start();
}