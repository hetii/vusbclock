#ifndef __ir_h_included__
#define __ir_h_included__

#include "irmp/irmp.h"
#include <avr/pgmspace.h>

#define CODES_TO_TRAIN 4

char str[120];
char *s;

void ir_init(uint8_t mode);
void ir_accept(void);
// return command id or -1 on unknow command.
int8_t ir_pool(void);

#endif

