#!/bin/bash 
#sh -c "echo 0 > /sys/class/rtc/rtc0/wakealarm" 
#sh -c "echo `date '+%s' -d '+ 2 minutes'` > /sys/class/rtc/rtc0/wakealarm"

wake_after_min=1
safe_margin_startup=0
wake_after_secs=$((wake_after_min*60))

stop_date=$(date +%s)
wake_date=$((stop_date+wake_after_secs-safe_margin_startup))
#wake_date=$stop_date

echo "STOP DATE" $stop_date `date -d @$stop_date +%F" "%T`
echo "WAKE DATE" $wake_date `date -d @$wake_date +%F" "%T`

echo 0 > /sys/class/rtc/rtc0/wakealarm
echo $wake_date > /sys/class/rtc/rtc0/wakealarm
cat /proc/driver/rtc
#shutdown -h now
