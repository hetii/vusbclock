#!/bin/bash

# BASIC SETTINGS change at wish

# Settings you have to look at
# IP_address tvheadend server
tvh_ip=192.???.???.???
# TvHeadend login and password
tvh_login=YOUR_LOGIN_NAME
tvh_password=YOUR_PASSWORD
# (post recording) process activity to be checked, leave blank to inactivate
#process_check=(PROCESS_A PROCESS_B)
process_check=

# Settings you might look at
# portnumbers to be checked for user activity (tvheadend 9981 + 9982 and samba 445)
port_numbers=(9981 9982 445)
# minimum time in seconds needed for consecutive shutdown AND startup
safe_margin_shutdown=600
# minimum time in seconds needed to start up the computer properly
safe_margin_startup=180
# minimum time in minutes not to shutdown after last user activity (watching tv/recording, up-/downloading files, booting the system)
idle_time_shutdown=30
# maximum time in hours not to wake up for updating EPG
epg_hours=48
# interval in seconds the script will check if the system should be shutdown
script_int=60

# END OF BASIC SETTINGS keep out from here ;-)

# set language
export LANG=C

# set session start
uptime_boot_sec=$(cat /proc/uptime | awk -F ' ' '{print$1}' | awk -F '.' '{print$1}')
boot_time=$(($(date +%s)-uptime_boot_sec))

# initial values do not change
recording=1

# check for shutdown
until [ $recording -eq 0 ];do

#     initial values do not change
    recording=0
    shutdown_timer=$idle_time_shutdown
    ping_array=()

#    countdown to shutdown
    until [ $shutdown_timer -eq 0 ];do
        shutdown_timer=$((shutdown_timer-1))

#        check for server connection tvheadend samba or any other server according to settings above
        status_netstat=1
        until [ $status_netstat -eq 0 ];do
            status_netstat=0
            sleep $script_int
            for i in $( echo "${port_numbers[*]}"); do
                port_no=$i
                for i in $(echo $(netstat -n | grep -i "ESTABLISHED" | grep "$tvh_ip:$port_no" |  awk -F ':' '{print ":"$2}' | sed 's/:'$port_no'\|[ ]//g')); do
                    shutdown_timer=$idle_time_shutdown
                    status_netstat=1
                    ping_array[$(echo "${#ping_array[*]}")]=$i
                done
            done
            ping_array=($(echo "${ping_array[*]}" | sed 's/[ ]/\n/g' | awk '!a[$0]++' ))
        done
        for i in $(echo "${ping_array[*]}"); do
            if [ $( ping -c1 $i | grep "received" | awk -F ',' '{print $2}' | awk '{print $1}' ) -eq 0 ]; then
                ping_array=($(echo "${ping_array[*]/$i/}"))
            fi
        done
        if [ $(echo "${#ping_array[*]}") -eq 0 -a $boot_time -lt $(($(date +%s)-idle_time_shutdown*60)) ]; then
            shutdown_timer=0
        fi
        if [ $(curl -s --user $tvh_login:$tvh_password http://127.0.0.1:9981/status.xml | grep "subscriptions" | awk -F '>' '{print $2}' | awk -F '<' '{print $1}') -ne 0 -a $shutdown_timer -eq 0 ]; then
            shutdown_timer=1
        fi

    done

#    check for active post recording processes
    total_processes=0
    for i in $( echo "${process_check[*]}"); do
        counter=$( ps -A | grep "$i" | wc -l )
        total_processes=$((total_processes+counter))
    done
    if [ $total_processes -ne 0 ]; then
        recording=$((recording+1))
    fi

#    check for active users
    if [ $(who | wc -l) -ne 0 ]; then
        recording=$((recording+1))
    fi

#    retrieve and calculate wake up data
    if [ $(curl -s --user $tvh_login:$tvh_password http://127.0.0.1:9981/status.xml | grep "subscriptions" | awk -F '>' '{print $2}' | awk -F '<' '{print $1}') -eq 0 ]; then
        wake_after_min=$((epg_hours*60))
        if [ $(curl -s --user $tvh_login:$tvh_password 127.0.0.1:9981/status.xml | grep "next" | awk -F '>' '{print $2}' | awk -F '<' '{print $1}' | wc -l) -gt 0 ]; then
            wake_after_min_temp=$(curl -s --user $tvh_login:$tvh_password 127.0.0.1:9981/status.xml | grep "next" | awk -F '>' '{print $2}' | awk -F '<' '{print $1}')
            if [ $wake_after_min -gt $wake_after_min_temp ]; then
                wake_after_min=$wake_after_min_temp
            fi
        fi
    else
        wake_after_min=0
    fi
    wake_after_secs=$((wake_after_min*60))

#    check safe margin shutdown
    if [ $safe_margin_shutdown -gt $wake_after_secs ]; then
        recording=$((recording+1))
    fi

done

# set RTC wake up time
stop_date=$(date +%s)
wake_date=$((stop_date+wake_after_secs-safe_margin_startup))
echo 0 > /sys/class/rtc/rtc0/wakealarm
echo $wake_date > /sys/class/rtc/rtc0/wakealarm

# shutdown computer
#sudo shutdown -h now
echo "SHUT ID DOWN!!!"
