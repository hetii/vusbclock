#!/bin/bash

install_files(){
  # cp -rp ../backups/autostart.sh /storage/.config/autostart.sh
  # cp -rp ../backups/vusbclient /storage/

  rm -rf /storage/.kodi/userdata/addon_data/service.tvheadend42/input
  rm -rf /storage/.kodi/userdata/addon_data/service.tvheadend42/channel
  rm -rf /storage/.kodi/userdata/addon_data/service.tvheadend42/passwd
  rm -rf /storage/.kodi/userdata/addon_data/service.tvheadend42/dvr

  cp -rp hts/input /storage/.kodi/userdata/addon_data/service.tvheadend42/input
  cp -rp hts/channel /storage/.kodi/userdata/addon_data/service.tvheadend42/channel
  cp -rp hts/passwd /storage/.kodi/userdata/addon_data/service.tvheadend42/passwd
  cp -rp hts/dvr /storage/.kodi/userdata/addon_data/service.tvheadend42/dvr
  cp -rp hts/profile/354b2256d1a78b8a4cd9681ff29183fd /storage/.kodi/userdata/addon_data/service.tvheadend42/profile/
  rm -rf /storage/.kodi/userdata/addon_data/service.tvheadend42/accesscontrol/*
  cp -rp hts/accesscontrol/* /storage/.kodi/userdata/addon_data/service.tvheadend42/accesscontrol/

  echo "Done :)"
}
install_files
